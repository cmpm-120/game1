class Start extends Scene {
    create() {
        this.engine.setTitle(this.engine.storyData.Title);
        this.engine.addChoice("Begin your story");
    }

    handleChoice() {
        this.engine.gotoScene(Location, this.engine.storyData.InitialLocation);
    }
}

let axeInShed = true;

class Location extends Scene {
    create(key) {

        let locationData = this.engine.storyData.Locations[key];
        this.engine.show(locationData.Body);

        if(locationData.Choices) {
            for(let choice of locationData.Choices) {
                this.engine.addChoice(choice.Text, choice);
            }

            let itemChoice = locationData.itemChoice;
            let exitChoice = locationData.exitChoice;
            switch(key) {
                case "Bedroom":
                case "Living Room":
                    this.engine.addChoice(itemChoice.Text, itemChoice);
                    break;
                case "Dining Room":
                    if(axeInShed) {
                        this.engine.addChoice(itemChoice.Text, itemChoice);
                    } else {this.engine.addChoice(exitChoice.Text, exitChoice)}
                    break;
                case "Shed":
                    if(axeInShed) {this.engine.addChoice(itemChoice.Text, itemChoice)}
                    break;
                default:
                    break;
            }
        } else {
            this.engine.addChoice("The end.")
        }
    }

    handleChoice(choice) {
        if(choice) {
            let choiceIsItem = false;
            switch(choice.Target) {
                case "Axe":
                    axeInShed = false;
                case "Safe":
                case "onTV":
                case "Boards":
                case "Escape":
                    choiceIsItem = true;
                default:
                    break;
            }
            if(choiceIsItem) {
                this.engine.show("&gt; "+choice.Text);
                this.engine.gotoScene(Item, choice.Target);
            } else {
                this.engine.show("&gt; "+choice.Text);
                this.engine.gotoScene(Location, choice.Target);
            }
        } else {
            this.engine.gotoScene(End);
        }
    }
}

class Item extends Scene {
    create(key) {
        let itemData = this.engine.storyData.Items[key];
        this.engine.show(itemData.Body);

        if(itemData.Choices) {
            for(let choice of itemData.Choices) {
                this.engine.addChoice(choice.Text, choice);
            }
        } else {
            this.engine.addChoice("The end.")
        }
    }

    handleChoice(choice) {
        if(choice) {
            switch(choice.Target) {
                case "continueTV1":
                case "continueTV2":
                case "continueTV3":
                case "offTV":
                    this.engine.show("&gt; "+choice.Text);
                    this.engine.gotoScene(Item, choice.Target);
                    break;
                default:
                    this.engine.show("&gt; "+choice.Text);
                    this.engine.gotoScene(Location, choice.Target);
                    break;
            }
        } else {
            this.engine.gotoScene(End);
        }
    }
}

class End extends Scene {
    create() {
        this.engine.show("<hr>");
        this.engine.show(this.engine.storyData.Credits);
    }
}

Engine.load(Start, 'myStory.json');